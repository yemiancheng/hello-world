# hello world
## desc

your desc here

## deps
- [x] xxx

## apis

- [x] xxx

```sh
cat src/index.sh |  grep "function " | sed "s/function */- [x] /g"  | sed "s/(.*) *{//g"
```

## feats

- [x] basic curd load lib

## usage

### how to use for poduction?

```sh
# get the code

# run the index file
# ./src/index.sh

# or import to your sh file
# source /path/to/the/index file

# simple usage

```

### how to use for developer?

```sh
# get the code

# run test
# ./test/index.sh
#2 get some fail test
#./test/index.sh | grep "it is false"
```

## author

yemiancheng <ymc.github@gmail.com>

## license

MIT
